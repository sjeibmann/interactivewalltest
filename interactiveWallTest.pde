// - Super Fast Blur v1.1 by Mario Klingemann <http://incubator.quasimondo.com>
// - BlobDetection library

import processing.video.*;
import blobDetection.*;

Capture cam;
BlobDetection theBlobDetection;
PImage img;
PImage oldCompFrame;
boolean newFrame=false;
int captureRate = 30;
int displayRate = 15;
float thres = 0.5f;
float diffThres = 0.3f;
int blurRad = 3;
int bg = 255;
color fillCol = color(0,0,0);
PShape form;
float cs;
int circMin = 0;
int circMax = 15;
boolean compFrameSet = false;
boolean showBlob = false;
boolean showBubbles = true;
boolean showConsole = false;
boolean showVid = false;
boolean posDis = true;
boolean bgSub = false;


// ==================================================
// setup()
// ==================================================
void setup()
{
  //size(640, 480); //debugging
  
  fullScreen(P2D,1); // if running on internal or external display
  
  // Capture
  cam = new Capture(this, 40*4, 30*4, captureRate); //use with internal camera
  //cam = new Capture(this, 40*4, 30*4, "Logitech Camera", captureRate); //use with external camera
  cam.start();// Comment this line if you use Processing 1.5
        
  // BlobDetection
  // img which will be sent to detection (a smaller copy of the cam frame);
  img = new PImage(80,60); 
  
  theBlobDetection = new BlobDetection(img.width, img.height);
  theBlobDetection.setPosDiscrimination(posDis);
  theBlobDetection.setThreshold(thres); // will detect bright areas whose luminosity > 0.2f;
  //theBlobDetection.computeTriangles();
  noCursor();
  //blobArray = new int[width*height];
    //frameRate(displayRate);
}

// ==================================================
// captureEvent()
// ==================================================
void captureEvent(Capture cam)
{
  cam.read();
  newFrame = true;
}

// ==================================================
// draw()
// ==================================================
void draw()
{
  if (newFrame)
  {
    newFrame=false;
    background(bg);
    img.copy(cam, 0, 0, cam.width, cam.height, 
        0, 0, img.width, img.height);
    fastblur(img, blurRad);
    if(bgSub){
      backgroundSubtract(img);
    }
    theBlobDetection.computeBlobs(img.pixels);
    drawBlobsAndEdges();
    if(showConsole){
      drawConsole();
    }
  }
  if(showVid){
    image(cam,(width-(width/10)),0,width/10,height/10);
  }
}



void backgroundSubtract(PImage compFrame){
 if(!compFrameSet){
   oldCompFrame = compFrame;
   compFrameSet = true;
 }else{
   compFrame.loadPixels();
   for(int i=0; i<compFrame.pixels.length-1;i++){
     if(compFrame.pixels[i] <= (oldCompFrame.pixels[i]-diffThres) || compFrame.pixels[i] >= (oldCompFrame.pixels[i]+diffThres)){
       img.pixels[i] = color(0,0,0);
     }
   }
   oldCompFrame = compFrame;
 }
}
  


// ==================================================
// drawBlobsAndEdges()
// ==================================================
void drawBlobsAndEdges()
{
  //noFill();
  Blob b;
  EdgeVertex eA,eB;
  for (int n=0 ; n<theBlobDetection.getBlobNb() ; n++)
  {
    b=theBlobDetection.getBlob(n);
    if (b!=null)
    {
      // Edges
      //if (drawEdges)
      {
        //strokeWeight(2);
        //stroke(0,255,0);
        noStroke();
        fill(fillCol);
        form = createShape();
        form.beginShape();
        //b.getEdgeNb() = sort(b.getEdgeNb());
        for (int m=0;m<b.getEdgeNb();m++)
        {
          eA = b.getEdgeVertexA(m);
          eB = b.getEdgeVertexB(m);
          if (eA !=null && eB !=null)
          //if (eA !=null )
            //line(width-(eA.x*width), eA.y*height,
            //     width-(eB.x*width), eB.y*height
            //     );
            form.vertex(width-(eA.x*width), eA.y*height);
            form.vertex(width-(eB.x*width), eB.y*height);
        }
        form.endShape();
        shape(form,0,0);
        loadPixels();
        if(!showBlob){
          noStroke();
          fill(bg);
          rect(0,0,width,height);
        }
        calcBubbles();
      }      
    }
  }
}

void calcBubbles(){
  //loadPixels();
  if(showBubbles){
    for(int blobPixel=0; blobPixel<=pixels.length-1; blobPixel+=random(100,250)){
      if(pixels[blobPixel] == fillCol){
      //pixels[blobPixel] = color(random(0,102),random(204,255),255);
      fill(random(0,102),random(204,255),255);
      cs = random(circMin,circMax);
      ellipseMode(CENTER);
      ellipse(getXCoord(blobPixel,width),getYCoord(blobPixel,width),cs,cs);
      }
    }
  }
}

int getXCoord(int index, int w){
 int x = (index%w);
 return x;
}

int getYCoord(int index, int w){
 int y = (index/w);
 return y;
}

void drawConsole(){
  pushMatrix();
  fill(0);
  rect(0,0,400,120);
  int nb = theBlobDetection.getBlobNb(); 
  fill(255);
  textSize(14);
  text("number of blobs: " + nb, 10,14);
  text("threshold: "  + thres, 10,30);
  text("image blur: "  + blurRad, 10,46);
  text("positive discrimination: "  + posDis, 10,62);
  text("video mode: "  + showVid, 10, 78);
  text("frame rate: "  + frameRate, 10,94);
  text("circ diameter range: "  + circMin + " - " + circMax, 10,110);
  popMatrix();
}


void keyPressed(){
  //println(key);
  if(key == 'f' || key == 'F'){
    showBlob = !showBlob;
  }
  if(key == 'b' || key == 'B'){
    showBubbles = !showBubbles;
  }
  if(key == 'c' || key == 'C'){
    showConsole = !showConsole;
  }
  if(key == 'v' || key == 'V'){
    showVid = !showVid;
  }
  if(key == 'p' || key == 'P'){
    posDis = !posDis;
    theBlobDetection.setPosDiscrimination(posDis);
  }
  if(key == ',' || key == '<'){
    diffThres -= 0.1;
  }
  if(key == '.' || key == '>'){
    diffThres += 0.1;
  }
  if(key == 'b' || key == 'B'){
    bgSub = !bgSub;
  }
  if(key == '1'){
    blurRad -= 1;
  }
  if(key == '2'){
    blurRad += 1;
  }
  if(key == CODED){
    if(keyCode == UP){
      thres += 0.1f;
    }
    if(keyCode == DOWN){
      thres -= 0.1f;
    }
    if(keyCode == LEFT){
      circMin -= 1;
      circMax -= 1;
    }
    if(keyCode == RIGHT){
      circMin += 1;
      circMax += 1;
    }
    theBlobDetection.setThreshold(thres);
  }
}

// ==================================================
// Super Fast Blur v1.1
// by Mario Klingemann 
// <http://incubator.quasimondo.com>
// ==================================================
void fastblur(PImage img,int radius)
{
 if (radius<1){
    return;
  }
  int w=img.width;
  int h=img.height;
  int wm=w-1;
  int hm=h-1;
  int wh=w*h;
  int div=radius+radius+1;
  int r[]=new int[wh];
  int g[]=new int[wh];
  int b[]=new int[wh];
  int rsum,gsum,bsum,x,y,i,p,p1,p2,yp,yi,yw;
  int vmin[] = new int[max(w,h)];
  int vmax[] = new int[max(w,h)];
  int[] pix=img.pixels;
  int dv[]=new int[256*div];
  for (i=0;i<256*div;i++){
    dv[i]=(i/div);
  }

  yw=yi=0;

  for (y=0;y<h;y++){
    rsum=gsum=bsum=0;
    for(i=-radius;i<=radius;i++){
      p=pix[yi+min(wm,max(i,0))];
      rsum+=(p & 0xff0000)>>16;
      gsum+=(p & 0x00ff00)>>8;
      bsum+= p & 0x0000ff;
    }
    for (x=0;x<w;x++){

      r[yi]=dv[rsum];
      g[yi]=dv[gsum];
      b[yi]=dv[bsum];

      if(y==0){
        vmin[x]=min(x+radius+1,wm);
        vmax[x]=max(x-radius,0);
      }
      p1=pix[yw+vmin[x]];
      p2=pix[yw+vmax[x]];

      rsum+=((p1 & 0xff0000)-(p2 & 0xff0000))>>16;
      gsum+=((p1 & 0x00ff00)-(p2 & 0x00ff00))>>8;
      bsum+= (p1 & 0x0000ff)-(p2 & 0x0000ff);
      yi++;
    }
    yw+=w;
  }

  for (x=0;x<w;x++){
    rsum=gsum=bsum=0;
    yp=-radius*w;
    for(i=-radius;i<=radius;i++){
      yi=max(0,yp)+x;
      rsum+=r[yi];
      gsum+=g[yi];
      bsum+=b[yi];
      yp+=w;
    }
    yi=x;
    for (y=0;y<h;y++){
      pix[yi]=0xff000000 | (dv[rsum]<<16) | (dv[gsum]<<8) | dv[bsum];
      if(x==0){
        vmin[y]=min(y+radius+1,hm)*w;
        vmax[y]=max(y-radius,0)*w;
      }
      p1=x+vmin[y];
      p2=x+vmax[y];

      rsum+=r[p1]-r[p2];
      gsum+=g[p1]-g[p2];
      bsum+=b[p1]-b[p2];

      yi+=w;
    }
  }

}